#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import datetime
import os
import random
from timeit import default_timer as timer
from astral import Astral
import pytz
from apscheduler.schedulers.background import BackgroundScheduler

audio_output = False
pooptime_timeout_alert = False
alerts_dir = './sounds/alerts/'
greetings_dir = './sounds/greetings/'

# Hardware config
SENSOR_PIN = 23 # PIR-module
RF_PIN = 2      # RF-sender
REED_PIN = 17	# REED-Switch
SENDER_ID = 12325261 # Restrict RF sockets to signals with this SENDER_ID, Set this up with your RF-Sockets using chacon_send

# Software config
gracetime = 25 # Time in seconds befor poopalert happens after door is shut
no_motion_timeout = 15 # Time in seconds before no motion switch off happens
sound_to_light_delay = 1 # Time in seconds between sound and light commands in pooptime_alert
SENDER_ID = 12325261 # Restrict RF sockets to signals with this SENDER_ID,

# Region config for correct sunevent times
region_name = 'Europe'
city_name = 'Berlin'

# Global variables
global start # Timestamp when door is closed to calculate pooping_time
global poop_alert_flag # Flag when alert was triggered for no unwanted second triggers
global motion_flag # Flag when motion was detected
global door_flag # State of doof. True = closed, False = open. Currently not in use
global nighttimeflag # only True for if current time is not within current day's sunrise and sunset
global sun # captures Sun event times
global motioncounter

# Init GPIO-Pins with threaded callbacks. PIR will put out high if motion is
# detected and switch back to low if no motion is detected within a certain time.
# Timespan is defined on the PIR-module itself.
# Detection of Signal edges High -> Low (FALLING) and Low -> High (RISING)
# leads to threaded code execution in BOTH cases.

# Settung up timezone and astral object
tz_BE = pytz.timezone('%s/%s' % (region_name, city_name))
a = Astral()
a.solar_depression = 'civil'
city = a[city_name]
sun = city.sun(date = datetime.date.today(), local = True)
print ('Information for %s/%s\n' % (city_name, city.region))
timezone = city.timezone
print('Timezone: %s' % timezone)
print('Latitude: %.02f; Longitude: %.02f\n' % (city.latitude, city.longitude))

# Running update tasks for daytime flag
sched = BackgroundScheduler()

def update_sunevent_times():
    print('update sun event times...')
    sun = city.sun(date = datetime.date.today(), local = True)
    print('Dawn:    %s' % str(sun['dawn']))
    print('Sunrise: %s' % str(sun['sunrise']))
    print('Noon:    %s' % str(sun['noon']))
    print('Sunset:  %s' % str(sun['sunset']))
    print('Dusk:    %s' % str(sun['dusk']))

def nighttimecheck():
    global nighttimeflag
    current_time = datetime.datetime.now(tz_BE).strftime('%Y-%m-%d %H:%M:%S')
    print ('current time: %s' % current_time)
    sunrise_time = str(sun['sunrise'])[:str(sun['sunrise']).rfind("+")]
    sunset_time = str(sun['sunset'])[:str(sun['sunset']).rfind("+")]
    print ('sunrise time: %s' % sunrise_time)
    print ('sunset time: %s' % sunset_time)

    if (current_time > sunrise_time and current_time < sunset_time):
        print('daytime')
        nighttimeflag = False
    else:
        print('nighttime')
        nighttimeflag = True

def edge_detected(PIR):
    global motion_flag
    # Check if PIR-module sends HIGH of LOW
    if GPIO.input(SENSOR_PIN) == 1:
        motion_flag = True
        motion_detected()
    else:
        motion_flag = False
        no_motion()

def door_movement(REED):
    print ('Door movement detected...')
    if GPIO.input(REED) == 0:
        door_opened()
    else:
        door_closed()

def door_closed():
    global poop_alert_flag
    global motioncounter
    print ('Door closed...')
    door_flag = True
    poop_alert_flag = False
    motioncouner = 1
    # Rising Edge calls function, greeting mp3 is played

    start = timer() # Starttime befor poopalert happens
    if GPIO.input(SENSOR_PIN) == 1:
        if audio_output == True:
            file = random.choice(os.listdir(greetings_dir))
            print ('playing file %s' % file)
            os.system('omxplayer %s%s &' % (greetings_dir, file))
            start = timer() # Starts time period befor something happens

def door_opened():
    global door_flag
    door_flag = False
    global motioncounter
    global pooping_time
    global poop_alert_flag
    print ('Door opened...')
    pooping_time = 0
    motioncounter = 0
    poop_alert_flag = False
    os.system(rfcommand(3, 0))
    os.system('killall omxplayer.bin &')

def time_elapsed(starttime):
    return (timer()-starttime)

def pooptime_alert():
    GPIO.remove_event_detect(REED_PIN)
    poop_alert_flag = True
    os.system('killall omxplayer.bin &')
    if audio_output == True:
        file = random.choice(os.listdir(alerts_dir))
        print ('playing file %s' % file)
        os.system('omxplayer %s%s &' % (alerts_dir, file))
        time.sleep(sound_to_light_delay)
        os.system(rfcommand(3, 1))
    time.sleep(1)
    GPIO.add_event_detect(REED_PIN, GPIO.BOTH, callback = door_movement, bouncetime = 1000)

# Setup PIR-Pin
GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN)
GPIO.add_event_detect(SENSOR_PIN , GPIO.BOTH, callback=edge_detected, bouncetime = 100)

# Setup REED-Pin
GPIO.setup(REED_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(REED_PIN , GPIO.BOTH, callback=door_movement, bouncetime = 1000)

# Waiting for PIR-Idle on PIR-Pin
def initPIR(PIR):
    print ('Waiting for PIR-Idle...')
    print ('sending signal %s' % GPIO.input(PIR))
    os.system(rfcommand(2, GPIO.input(PIR)))
    time.sleep(0.2)

# PIR-module sends high
def motion_detected():
    global nighttimeflag
    global motioncounter
    motion_flag = True
    motioncounter = motioncounter + 1
    print ('%s - Motion detected!' % datetime.datetime.now())
    if nighttimeflag == True:
        os.system(rfcommand(2, 1))

# PIR-module sends low
def no_motion():
    global motion_flag
    motion_flag = False
    counter = 0
    while GPIO.input(SENSOR_PIN) == 0:
        time.sleep(1)
        counter+=1
        if GPIO.input(REED_PIN) == 1:
            if counter > no_motion_timeout and motioncounter < 3:
                print ('%s - No Motion...' % datetime.datetime.now())
                os.system(rfcommand(2, 0))
                os.system(rfcommand(3, 0))
                os.system('killall omxplayer.bin &')
                pooping_time = 0
            break
        if GPIO.input(REED_PIN) == 0:
            if counter > no_motion_timeout:
                print ('%s - No Motion...' % datetime.datetime.now())
                os.system(rfcommand(2, 0))
                os.system(rfcommand(3, 0))
                os.system('killall omxplayer.bin &')
                pooping_time = 0
            break
        break
    #motion_flag = True

# RF commands callable with devicenumber and on/off state (1/0)
def rfcommand(Device, on):
    print ('Device %s:' % Device)
    if on == True:
        command = 'sudo ./chacon_send %s %s %s on' % (RF_PIN, SENDER_ID, Device)
        #devicestate = 1
    else:
        command = 'sudo ./chacon_send %s %s %s off' % (RF_PIN, SENDER_ID, Device)
        #devicestate = 0
    return command

# Run
initPIR(SENSOR_PIN)
os.system(rfcommand(3,0)) # Reset Light 3

print('%s - Waiting for motion' % datetime.datetime.now())
poop_alert_flag = False
motion_flag = False
nighttimeflag = False
motioncounter = 0
#start = timer()
#pooping_time = 0

update_sunevent_times()
nighttimecheck()

sched.add_job(update_sunevent_times, 'interval', hours=3)
sched.add_job(nighttimecheck, 'interval', minutes=5)
sched.start()

while True:
    while nighttimeflag == True:
        print ('Reed Pin: %s' % GPIO.input(REED_PIN))
        print ('PIR Pin: %s' % GPIO.input(SENSOR_PIN))
        #print ('Pooping Time: %s' % pooping_time)
        start = timer()
        pooping_time = 0
        print('Motion Flag: %s' % motion_flag)
        print('Poopalert Flag: %s' % poop_alert_flag)
        print('Motioncounter: %s' % motioncounter)
        if GPIO.input(SENSOR_PIN) == 0 and GPIO.input(REED_PIN) == 0:
            motioncounter = 0
        while GPIO.input(REED_PIN) == 1 and motion_flag == True and poop_alert_flag == False: # Door closed & Movement detected & No poop alert going on
            #print ('Both active')
            #start = timer()
            pooping_time = time_elapsed(start)
            print ('Pooping time: %s', pooping_time)
            if pooping_time > gracetime:
                print ('pooping time alert')
                if pooptime_timeout_alert == True and GPIO.input(SENSOR_PIN) == 1:
                    poop_alert_flag = True
                    pooptime_alert()
            time.sleep(1)
        time.sleep(1)
    time.sleep(1)
sched.shutdown()

#except KeyboardInterrupt:
#    print "Quit..."
#    GPIO.cleanup() # Always good to clean up your mess
#    os.system('killall omxplayer')
