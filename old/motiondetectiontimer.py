#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import datetime
import os
from timeit import default_timer as timer

audio_output = False
pooptime_timeout_alert = False

# Input
SENSOR_PIN = 23 # PIR-module
RF_PIN = 2      # RF-sender
REED_PIN = 17	# REED-Switch
gracetime = 20 # Time in seconds befor something happens after door is shut

global start
global poop_alert_flag
global devicestate

SENDER_ID = 12325261 # Restrict RF sockets to signals with this SENDER_ID,

# Init GPIO-Pins with threaded callbacks. PIR will put out high if motion is
# detected and switch back to low if no motion is detected within a certain time.
# Timespan is defined on the PIR-module itself.
# Detection of Signal edges High -> Low (FALLING) and Low -> High (RISING)
# leads to threaded code execution in BOTH cases.

def edge_detected(PIR):
    # Check if PIR-module sends HIGH of LOW
    if GPIO.input(SENSOR_PIN) == 1:
        motion_detected()
    else:
        no_motion()

def door_movement(REED):
    print 'Door movement detected...'
    if GPIO.input(REED) == 0:
        door_opened(REED)
    else:
        door_closed(REED)

def door_closed(REED):
    global start
    print 'Door closed...'
    # Rising Edge calls function, greeting mp3 is playerd
    if GPIO.input(SENSOR_PIN) == 1:
        if audio_output == True:
            os.system('omxplayer sounds/greeting.mp3')
            start = timer() # Starts time period befor something happens
    return 0

def door_opened(REED):
    print 'Door opened...'
    global poop_alert_flag
    global pooping_time
    pooping_time = 0
    os.system(rfcommand(3, 0))
    poop_alert_flag = False

def time_elapsed():
    return (timer()-start)

def pooptime_alert():
    global poop_alert_flag
    if audio_output == True:
        os.system('omxplayer sounds/alert.mp3')
    os.system(rfcommand(3, 1))
    poop_alert_flag = True
    return 0;

GPIO.setmode(GPIO.BCM)

# Setup PIR-Pin
GPIO.setup(SENSOR_PIN, GPIO.IN)
GPIO.add_event_detect(SENSOR_PIN , GPIO.BOTH, callback=edge_detected)

# Setup REED-Pin
GPIO.setup(REED_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(REED_PIN , GPIO.BOTH, callback=door_movement, bouncetime = 1000)
#GPIO.add_event_detect(REED_PIN , GPIO.RISING, callback=door_closed, bouncetime = 1000)
#GPIO.add_event_detect(REED_PIN , GPIO.FALLING, callback=door_opened, bouncetime = 1000)

# Waiting for PIR-Idle on PIR-Pin
def initPIR(PIR):
    print"Waiting for PIR-Idle..."
    print ('sending signal %s' % GPIO.input(PIR))
    os.system(rfcommand(2, GPIO.input(PIR)))
    time.sleep(0.2)

# while GPIO.input(PIR) != 0:
 #       time.sleep(0.2)
#    rfcommand(2, 0)

# PIR-module sends high
def motion_detected():
    print "%s - Motion detected!" % datetime.datetime.now()
    os.system(rfcommand(2, 1))

# PIR-module sends low
def no_motion():
    time.sleep(3) # Low Signal of PIR has to stay low for 3 seconds for off commmand to be sent
    if GPIO.input(SENSOR_PIN) == 0:
        print "%s - No Motion..." % datetime.datetime.now()
        os.system(rfcommand(2, 0))

# RF commands callable with devicenumber and on/off state (1/0)
def rfcommand(Device, on):
    if on == True:
        command = 'sudo ./chacon_send %s %s %s on' % (RF_PIN, SENDER_ID, Device)
        devicestate = 1
    else:
        command = 'sudo ./chacon_send %s %s %s off' % (RF_PIN, SENDER_ID, Device)
        devicestate = 0
    return command

# Run
initPIR(SENSOR_PIN)
#print "PIR-module ready, sending off reset"
#os.system(rfcommand(2, 0)) # Reset

print "%s - Waiting for motion" %datetime.datetime.now()
poop_alert_flag = False
start = timer()
pooping_time = 0

while True:
    #print ('Reed Pin: %s' % GPIO.input(REED_PIN))
    #print ('PIR Pin: %s' % GPIO.input(SENSOR_PIN))
    #print ('Pooping Time: %s' % pooping_time)
    while GPIO.input(REED_PIN) == 1 and GPIO.input(SENSOR_PIN) == 1 and poop_alert_flag == False: # Door closed & Movement detected
	print 'Both active' 
        pooping_time = time_elapsed()
        print ('Pooping time: %s', pooping_time)
        if pooping_time > gracetime:
            print 'pooping time alert'
            if pooptime_timeout_alert == True:
                pooptime_alert()
        time.sleep(1)

    start = timer()
    pooping_time = 0 
    time.sleep(1)

#except KeyboardInterrupt:
 #   print "Quit..."
  #  GPIO.cleanup() # Always good to clean up your mess
