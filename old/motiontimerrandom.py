#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import datetime
import os
import random
from timeit import default_timer as timer

audio_output = True
pooptime_timeout_alert = True
alerts_dir = './sounds/alerts/'
greetings_dir = './sounds/greetings/'

# Hardware config
SENSOR_PIN = 23 # PIR-module
RF_PIN = 2      # RF-sender
REED_PIN = 17	# REED-Switch
SENDER_ID = 12325261 # Restrict RF sockets to signals with this SENDER_ID, Set this up with your RF-Sockets using chacon_send

# Software config
gracetime = 25 # Time in seconds befor poopalert happens after door is shut
no_motion_timeout = 15 # Time in seconds before no motion switch off happens
sound_to_light_delay = 1 # Time in seconds between sound and light commands in pooptime_alert

# Global variables
global start # Timestamp when door is closed to calculate pooping_time
global poop_alert_flag # Flag when alert was triggered for no unwanted second triggers
global motion_flag # Flag when motion was detected
global door_flag # State of doof. True = closed, False = open. Currently not in use

SENDER_ID = 12325261 # Restrict RF sockets to signals with this SENDER_ID,

# Init GPIO-Pins with threaded callbacks. PIR will put out high if motion is
# detected and switch back to low if no motion is detected within a certain time.
# Timespan is defined on the PIR-module itself.
# Detection of Signal edges High -> Low (FALLING) and Low -> High (RISING)
# leads to threaded code execution in BOTH cases.

def edge_detected(PIR):
    global motion_flag
    # Check if PIR-module sends HIGH of LOW
    if GPIO.input(SENSOR_PIN) == 1:
        motion_flag = True
        motion_detected()
    else:
        motion_flag = False
        no_motion()

def door_movement(REED):
    print ('Door movement detected...')
    if GPIO.input(REED) == 0:
        door_opened()
    else:
        door_closed()

def door_closed():
    global poop_alert_flag
    print ('Door closed...')
    door_flag = True
    poop_alert_flag = False
    # Rising Edge calls function, greeting mp3 is played

    start = timer() # Starttime befor poopalert happens
    if GPIO.input(SENSOR_PIN) == 1:
        if audio_output == True:
            file = random.choice(os.listdir(greetings_dir))
            print ('playing file %s' % file)
            os.system('omxplayer %s%s &' % (greetings_dir, file))
            start = timer() # Starts time period befor something happens

def door_opened():
    door_flag = False
    print ('Door opened...')
    pooping_time = 0
    poop_alert_flag = False
    os.system(rfcommand(3, 0))
    os.system('killall omxplayer.bin &')

def time_elapsed(starttime):
    return (timer()-starttime)

def pooptime_alert():
    GPIO.remove_event_detect(REED_PIN)
    poop_alert_flag = True
    os.system('killall omxplayer.bin &')
    if audio_output == True:
        file = random.choice(os.listdir(alerts_dir))
        print ('playing file %s' % file)
        os.system('omxplayer %s%s &' % (alerts_dir, file))
        time.sleep(sound_to_light_delay)
        os.system(rfcommand(3, 1))
    time.sleep(1)
    GPIO.add_event_detect(REED_PIN, GPIO.BOTH, callback = door_movement, bouncetime = 1000)

# Setup PIR-Pin
GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN)
GPIO.add_event_detect(SENSOR_PIN , GPIO.BOTH, callback=edge_detected, bouncetime = 100)

# Setup REED-Pin
GPIO.setup(REED_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(REED_PIN , GPIO.BOTH, callback=door_movement, bouncetime = 1000)

# Waiting for PIR-Idle on PIR-Pin
def initPIR(PIR):
    print ('Waiting for PIR-Idle...')
    print ('sending signal %s' % GPIO.input(PIR))
    os.system(rfcommand(2, GPIO.input(PIR)))
    time.sleep(0.2)

# PIR-module sends high
def motion_detected():
    motion_flag = True
    print ('%s - Motion detected!' % datetime.datetime.now())
    os.system(rfcommand(2, 1))

# PIR-module sends low
def no_motion():
    global motion_flag
    motion_flag = False
    counter = 0
    while GPIO.input(SENSOR_PIN) == 0:
        time.sleep(1)
        counter+=1
        if counter > no_motion_timeout:
            print ('%s - No Motion...' % datetime.datetime.now())
            os.system(rfcommand(2, 0))
            os.system(rfcommand(3, 0))
            os.system('killall omxplayer.bin &')
            pooping_time = 0
            break
    #motion_flag = True

# RF commands callable with devicenumber and on/off state (1/0)
def rfcommand(Device, on):
    print ('Device %s:' % Device)
    if on == True:
        command = 'sudo ./chacon_send %s %s %s on' % (RF_PIN, SENDER_ID, Device)
        devicestate = 1
    else:
        command = 'sudo ./chacon_send %s %s %s off' % (RF_PIN, SENDER_ID, Device)
        devicestate = 0
    return command

# Run
initPIR(SENSOR_PIN)
os.system(rfcommand(3,0)) # Reset Light 3

print('%s - Waiting for motion' % datetime.datetime.now())
poop_alert_flag = False
motion_flag = False
#start = timer()
#pooping_time = 0

while True:
    print ('Reed Pin: %s' % GPIO.input(REED_PIN))
    print ('PIR Pin: %s' % GPIO.input(SENSOR_PIN))
    #print ('Pooping Time: %s' % pooping_time)
    start = timer()
    pooping_time = 0
    print('Motion Flag: %s' % motion_flag)
    print('Poopalert Flag: %s' % poop_alert_flag)
    while GPIO.input(REED_PIN) == 1 and motion_flag == True and poop_alert_flag == False: # Door closed & Movement detected & No poop alert going on
        #print ('Both active')
        #start = timer()
        pooping_time = time_elapsed(start)
        print ('Pooping time: %s', pooping_time)
        if pooping_time > gracetime:
            print ('pooping time alert')
            if pooptime_timeout_alert == True and GPIO.input(SENSOR_PIN) == 1:
                poop_alert_flag = True
                pooptime_alert()
        time.sleep(1)
    time.sleep(1)

#except KeyboardInterrupt:
#    print "Quit..."
#    GPIO.cleanup() # Always good to clean up your mess
#    os.system('killall omxplayer')
