#!/usr/bin/python
import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers

MAGNET_GPIO = 17
GPIO.setup(MAGNET_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # GPIO Assign mod

while True:
    print GPIO.input(MAGNET_GPIO)

    sleep(0.5)
