#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import datetime
import os

# Input -  Use BCM PIN numbers
SENSOR_PIN = 23 # PIR-module
RF_PIN = 2      # RF-sender
SENDER_ID = 12325261 # Restrict RF sockets to signals with this SENDER_ID,

# Callback-function if edge is detected
def edge_detected():
    # Check if PIR-module sends HIGH of LOW
    if GPIO.input(SENSOR_PIN) == 1:
        motion_detected()
    else:
        no_motion()

# Init GPIO-Pins with threaded callbacks. PIR will put out high if motion is
# detected and switch back to low if no motion is detected within a certain time.
# Timespan is defined on the PIR-module itself.
# Detection of Signal edges High -> Low (FALLING) and Low -> High (RISING)
# leads to threaded code execution in BOTH cases.
GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN)
GPIO.add_event_detect(SENSOR_PIN , GPIO.BOTH, callback=edge_detected, bouncetime = 1000) # bouncetime allows software debouncing of PIR Signal. Usefull when receiving repeated signal changes in small timeperid. bouncetime will ignore signals after first recogniton for specified time in milliseconds

# Waiting for PIR-Idle on PIR-Pin
def initPIR(PIR):
    print"Waiting for PIR-Idle..."
    if GPIO.input(PIR) != 0:
        rfcommand(2, 1)
    while GPIO.input(PIR) != 0:
        time.sleep(0.2)

# PIR-module sends high
def motion_detected():
    print"%s - Motion detected!" % datetime.datetime.now()
    os.system(rfcommand(2, 1))

# PIR-module sends low
def no_motion():
    print "%s - No Motion..." % datetime.datetime.now()
    os.system(rfcommand(2, 0))

# RF commands callable with devicenumber and on/off state (1/0)
def rfcommand(Device, on):
    if on == True:
        command = 'sudo ./chacon_send %s %s %s on' % (RF_PIN, SENDER_ID, Device)
    else:
        command = 'sudo ./chacon_send %s %s %s off' % (RF_PIN, SENDER_ID, Device)
    return command

# Run
initPIR(SENSOR_PIN)
print "PIR-module ready, sending off reset"
os.system(rfcommand(2, 0)) # Reset

print "%s - Waiting for motion" %datetime.datetime.now()

# Loop
while True:
    time.sleep(0.2)

#except KeyboardInterrupt:
#    print "Quit..."
#
#GPIO.cleanup() # Always good to clean up your mess
