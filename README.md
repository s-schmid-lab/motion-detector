This project is about turning a Raspberry Pi into a motion detector, using a PIR-
module. It will control RF 433 Commands.

Plug the RF 433MHz transmitter
The radio transmitter has 3 pins: one power pin, one DATA pin and a Ground pin:

**HARDWARE SETUP**

* RF-Transmitter
The power pin can be plugged to the remaining 3v3 power pin on the Pi
(pin number 1 in the pinout schema). We will use the pin number 13 (GPIO 2) for
DATA, and pin 14 for the ground. We plug the transmitter to the Raspberry Pi
using the female/female cables.

* PIR-Module
The PIR-module can be connected to 5V and ground. The digital out pin is
connected to BCM 23 (pin 16).


**SOFTWARE SETUP**

* Install the Raspbian Stretch Lite distribution
* Configure Wifi access: Add the following file wpa_supplicant.conf at the root of the boot volume of the SD card (do not forget to update the ssid and psk keys as well as the country key):
```
country=DE
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
    ssid="Your WiFi network name"
    psk="Your WiFi network password"
}
```

* **Enable SSH:** Enable SSH by adding an empty file ssh at the root of the boot volume on your SD card. Insert the SD card into the Raspberry. It will boot in about 20 seconds. You can now access your Raspberry from your computer using ssh pi@raspberrypi.local
Note: If you have several Raspberry Pi’s on the same network, it is probably a good idea to change their hostname or connect to them using their IP.
From now on, the next installation commands will have to be run from the Raspberry Pi, through ssh.

* **Setup the radio transmitter: **
In order to command the 433MHz radio transmitter, we first need to install WiringPi. It is a PIN based GPIO access library which will be used by our main program in order to access and control the DATA GPIO pin used by the transmitter. Install WiringPi witch:

`sudo apt-get install wiringpi`

Now that WiringPi is installed, we can install the program that will replicate what the DIO remote control does when you press on and off. This program is written in C and largely inspired by this article (in French) from Idleman. Here are the steps to download the C program and compile it:

```
cd
mkdir lights_commands && cd lights_commands
wget https://raw.githubusercontent.com/adrienball/smart-lights/master/chacon_send.cpp
g++ -o chacon_send chacon_send.cpp -lwiringPi
```

This will generate a binary named chacon_send which we will use to send on and off signals to our DIO connected plug:

`sudo ./chacon_send <wiringPI pin> <controller_code> <outlet_code> <on|off>`

* wiringPI pin: this corresponds to the wiringPi pin number on which we plugged the DATA GPIO pin of the radio transmitter. We used the GPIO pin 13 which corresponds to the wiringPi number 2. If you used another pin, you can refer to the wiringPi mapping, or the table below, to get the corresponding number.

* controller_code: this is a transmitter id number that we attribute arbitrarily to the RaspberryPi. It allows the power outlet to exclusively accept commands from this transmitter.
* outlet_code: this is a receiver code that you attribute arbitrarily to the DIO outlet that you want to control.
* on|off: the desired state of the outlet.

The transmitter/receiver synchronisation happens when you plug the power outlet. The Chacon power outlet contains a signal synchronisation module which means that, when you plug the outlet, there will be a short time of 5 seconds during which it will listen for any emitted signal and will remember its attribution codes (controller_code and outlet_code). Thus, for each plug you want to control, you will have to plug it and, within 5 seconds, send a signal using the command described just before (using a different outlet_code for each outlet).
The power outlet will make some characteristic “clic-clac” sounds to let you know that it has properly recognised and memorised your signal.
At this point, you can try the radio transmitter by plugging your power outlet and run:

`sudo ./chacon_send 2 12325261 1 on`

If everything went well, your power outlet should make a clic as it switches on.

For sunchecker you need Astral python module installed:
https://astral.readthedocs.io/en/stable/index.html#sun


Execute script on startup and auto-restart with systemctl and corresponding .service file
